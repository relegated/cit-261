function loop() {
    let numToLoop = document.getElementById("numTimes").value;
    let textToDisplay = "";

    for (var i = 0; i < numToLoop; i++) {
        textToDisplay += "Hello World\n";
    }

    document.getElementById("loopOutput").innerHTML = textToDisplay;
}

function condition() {
    let show = "Show";
    let hide = "Hide";
    let textIsDisplayed = hide.localeCompare(document.getElementById("buttonCondition").innerHTML);
    
    if (textIsDisplayed) {
        document.getElementById("conditionOutput").innerHTML = "";
    } 
    else {
        document.getElementById("conditionOutput").innerHTML = "This text can be toggled on and off";
    }
}

function func() {
    alert("Congratulations, you called the alert function!");
}

function double() {
    let x = document.getElementById("variable").value;
    x *= 2;
    document.getElementById("variableOutput").innerHTML = x;
}

function digitsOfPi(precision) {
    document.getElementById("parameterOutput").innerHTML = Math.PI.toPrecision(precision);
}

function getArrayValueAt() {
    let x = document.getElementById("arrayIndex").value;
    let zeldaCharacters = ["Link", "Zelda", "Impa", "Ruto", "Darunia", "Ganon"];
    document.getElementById("arrayOutput").innerHTML = zeldaCharacters[x];
}

function getAssociativeArrayValueAt() {
    let selectedValue = document.getElementById("comboBoxDungeon").value;
    let zeldaItems = [];
    zeldaItems["deku"] = "Slingshot";
    zeldaItems["mines"] = "Bombs";
    zeldaItems["belly"] = "Boomerang";
    zeldaItems["forest"] = "Fairy Bow";
    zeldaItems["mount"] = "Megaton Hammer";
    zeldaItems["lake"] = "Longshot";
    zeldaItems["shadow"] = "Hover Boots";
    zeldaItems["spirit"] = "Mirror Shield";

    document.getElementById("AssociativeArrayOutput").innerHTML = zeldaItems[selectedValue];
}